package org.bitbucket.anuarkaliyev23.mnc.libraries;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Calculator {
    static Logger LOGGER = LoggerFactory.getLogger(Calculator.class);

    static int add(int a, int b) {
        LOGGER.info("Adding a + b = " + a + b);
        return a + b;
    }
    static int minus(int a, int b) {
        return a - b;
    }
    static int multiply(int a, int b) {
        return a * b;
    }
    static int divide(int a, int b) {
        return a / b;
    }
}
