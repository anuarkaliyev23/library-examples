package org.bitbucket.anuarkaliyev23.mnc.libraries;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add() {
        assertEquals(4, Calculator.add(2, 2));
        assertEquals(5, Calculator.add(2, 3));
    }

    @Test
    void divide() {
        assertThrows(ArithmeticException.class, () -> {
            Calculator.divide(4, 0);
        });
    }

    @Test
    void minus() {
        assertEquals(2, Calculator.minus(5, 3));
    }

}